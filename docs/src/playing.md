# Playing

<!-- toc -->

When the game is successfully installed, the `Game` tab allows you to choose 
from available presets, specify launch parameters and — more importantly — 
launch the game!

[![Game tab](screens/launch.th.png)](screens/launch.png)

## Preset Selection

Initially, the launcher creates a `default` preset with the packages you selected
during installation. You can edit this preset or [add more presets](./packs.md),
and they show up in the preset selection list. You can also have a quick look
at the packages enabled in the preset.

## Launching Game

Once a preset is selected, you can start the game using the `Launch RVGL`
button. The Launcher UI remains minimized during gameplay and restores itself
once you close the game.

> You won't be able to launch the game during package updates. You should always
keep the game closed during the update process.

## Launch Parameters 

Advanced users can specify launch parameters in the provided text box. Eg.,
`-window 1280 720 -nointro -sload`. These parameters are added on top of base
parameters required to launch the game. For a full list of supported parameters,
see the [RVGL docs](https://re-volt.gitlab.io/rvgl-docs/general.html#launch-parameters).

