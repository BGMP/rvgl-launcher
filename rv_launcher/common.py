import sys
import os
import platform
import threading
import requests
from appdirs import *


APP_NAME = "rvmm"
APP_TITLE = "RVGL Launcher"
APP_ID = "bd67d014" # unique identifier
APP_URI = "rvmm://"

DATA_DIR = user_data_dir(APP_NAME, False)
CONFIG_DIR = user_config_dir(APP_NAME, False)
PACKAGE_URL = "https://distribute.re-volt.io/packs/"
PACKAGELIST_URL = "https://distribute.re-volt.io/packages.json"
REPOLIST_URL = "https://distribute.re-volt.io/repos/"
RELEASE_URL = "https://distribute.re-volt.io/releases/"

RVGL_URL = "https://rvgl.org/downloads/"
APPINFO_URL = RVGL_URL + "rvgl_launcher.json"

RVIO_URL = "https://re-volt.io/"
RVIO_EVENTS_URL = RVIO_URL + "events-data"

CONFIG = {
    "install-type": "standard",
    "data-dir": DATA_DIR,
    "launch-params": "",
    "installed": False,
    "uri-registered": False,
    "lobby-registered": False,
    "repos": [],
    "disabled-repos": [],
    "install-recipe": "original",
    "default-recipe": "default",
    "show-console": False,
    "fix-cases": True
}

PLATFORM_LIST = ["win32", "win64", "linux", "macos"]

if sys.platform == "linux":
    PLATFORM = "linux"
elif sys.platform == "win32":
    if platform.architecture()[0] == "64bit":
        PLATFORM = "win64"
    else:
        PLATFORM = "win32"
elif sys.platform == "darwin":
    PLATFORM = "macos"
else:
    print("Unsupported platform.")
    exit()


repo = None
event_catalog = None
log_file = None
recipes = {}

close_event = threading.Event()
close_event.clear()

message_event = threading.Event()
message_event.clear()

session = requests.Session()


""" Returns whether application is run from a frozen bundle """
def is_frozen():
    return getattr(sys, "frozen", False)


""" Returns the application base folder """
def get_app_path():
    if is_frozen():
        return os.path.dirname(sys.executable)
    else:
        return sys.path[0]


""" Returns the path to assets bundled with the application """
def get_dist_path():
    if sys.platform == "darwin" and is_frozen():
        return os.path.abspath(os.path.join(os.pardir, "Resources"))
    elif sys.platform in ["win32", "darwin"] and not is_frozen():
        return os.path.join("dist", PLATFORM)
    else:
        return ""


""" Returns the command and args used to launch the application """
def get_app_command():
    if is_frozen():
        return sys.executable, ""
    else:
        name = os.path.join(get_app_path(), "rvgl_launcher.py")
        return sys.executable, name

